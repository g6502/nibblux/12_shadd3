library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;

entity SHADD3_tb is 
end SHADD3_tb;

architecture test of SHADD3_tb is
	signal num	:	std_logic_vector(7 downto 0) := (others => '0');
	signal BCD	:	std_logic_vector(11 downto 0);
	
begin
	uu0 : entity work.SHADD3
		port map(num => num,
					bcd => bcd);
	process
	begin
		for i in 0 to 255 loop
			num <= std_logic_vector(to_unsigned(i,num'length));
			wait for 100 ps;
		end loop;
	end process;
end test;